/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "main.h"
#include "stdio.h"
#include "uart_driver.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum bmp280_register_address_{
	BMP280_REG_ADDR_ERROR					= 0x00,
	BMP280_REG_ADDR_CAL_START_ADDR			= 0x88,
	BMP280_REG_ADDR_ID						= 0xD0,
	BMP280_REG_ADDR_RESET					= 0xE0,
	BMP280_REG_ADDR_STATUS					= 0xF3,
	BMP280_REG_ADDR_CTRL_MEAS				= 0xF4,
	BMP280_REG_ADDR_CONFIG					= 0xF5,
	BMP280_REG_ADDR_PRESS_MSB				= 0xF7,
	BMP280_REG_ADDR_PRESS_LSB				= 0xF8,
	BMP280_REG_ADDR_PRESS_XLSB				= 0xF9,
	BMP280_REG_ADDR_TEMP_MSB				= 0xFA,
	BMP280_REG_ADDR_TEMP_LSB				= 0xFB,
	BMP280_REG_ADDR_TEMP_XLSB				= 0xFC,

		}bmp280_register_address_t;

typedef struct bmp_calibration_data_t {
	uint16_t 	dig_T1;
	int16_t 	dig_T2;
	int16_t 	dig_T3;
	uint16_t 	dig_P1;
	int16_t 	dig_P2;
	int16_t 	dig_P3;
	int16_t 	dig_P4;
	int16_t 	dig_P5;
	int16_t 	dig_P6;
	int16_t 	dig_P7;
	int16_t 	dig_P8;
	int16_t 	dig_P9;
}bmp_calibration_data_t;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
 ADC_HandleTypeDef hadc1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim1;
DMA_HandleTypeDef hdma_tim1_ch1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
 ADC_HandleTypeDef hadc1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim1;
DMA_HandleTypeDef hdma_tim1_ch1;
uart_driver_t uart_driver;

UART_HandleTypeDef huart1;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM1_Init(void);
static void MX_ADC1_Init(void);
static void MX_SPI1_Init(void);
/* USER CODE BEGIN PFP */
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM1_Init(void);
static void MX_ADC1_Init(void);
static void MX_SPI1_Init(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

//////************TRANSMIT UART********////////////////
int _write(int file, char *ptr, int len)
{
	HAL_UART_Transmit(&huart1, (uint8_t *) ptr, len, 10);
	return len;
}
///////////////////////////////////////////////////////


///////////////***********HEARTBEAT************///////////////////
void heartbeat(void)
{
	static uint32_t tick = 0;
	if (tick < HAL_GetTick())
	{
		tick = HAL_GetTick()+1000;
		HAL_ADC_Start(&hadc1);
		HAL_GPIO_TogglePin(Heartbeat_led_GPIO_Port, Heartbeat_led_Pin);
	}
}
/////////////////////////////////////////////////////////////////


///////////////***********READ TEMPERATURE************///////////////////

float BMP280_read_temperature(void)
{
	uint8_t spi_tx_data= BMP280_REG_ADDR_TEMP_MSB;
	uint8_t spi_rx_data[3]={0x00};
	int32_t raw_temperature = 0;
	uint16_t dig_T1=0b0110100110100011;
	int16_t dig_T2=0b0110101001000000;
	int16_t dig_T3=0b1111110000011000;
	float temperature = 0;
	HAL_GPIO_WritePin(bmp280_cs_GPIO_Port, bmp280_cs_Pin, GPIO_PIN_RESET);
	if (HAL_SPI_Transmit(&hspi1, &spi_tx_data, 1, 100)==HAL_OK)
	{
	  if (HAL_SPI_Receive(&hspi1, spi_rx_data, 3, 100)== HAL_OK)
	  {
		  raw_temperature= (int32_t)(spi_rx_data[2] & 0b00001111) | (spi_rx_data[1]<<4) | (spi_rx_data[0]<<12);
		  double var1 = 0;
		  double var2 = 0;
		  var1 = (((double)raw_temperature) / 16384.0 - ((double)dig_T1) / 1024.0) * ((double)dig_T2);
		  var2 = ((double)raw_temperature) / 131072.0 - ((double)dig_T1) / 8192.0;
		  var2 = (var2*var2)*((double)dig_T3);
		  temperature = (float)(var1 + var2) / 5120.0;
	  }
	}

	HAL_GPIO_WritePin(bmp280_cs_GPIO_Port, bmp280_cs_Pin, GPIO_PIN_SET);
	return temperature ;
}

////////*************DMA_CHALLENGE*************//////////
void pwm_dma(float pwm_init, float pwm_fin)
{
	#define BUFF_SIZE 10000
	float duty_init=pwm_init/100;
	float duty_fin=pwm_fin/100;
	float Max_valuescale= BUFF_SIZE*duty_fin;
	float Min_valuescale= BUFF_SIZE*duty_init;
	float step=(Max_valuescale-Min_valuescale)/BUFF_SIZE;
	uint16_t pwm_dma_buff[BUFF_SIZE]={0};
	for (uint16_t index=0; index<BUFF_SIZE; index++)
	{
		pwm_dma_buff[index]=(Min_valuescale)+(step*index);

	}

	HAL_TIMEx_PWMN_Start_DMA(&htim1, TIM_CHANNEL_1, (uint32_t *)pwm_dma_buff, BUFF_SIZE);
}


////////////////////////////////////////////////////////////////////////
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  HAL_Init();
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  SystemClock_Config();
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_ADC1_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */

  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_ADC1_Init();
  MX_SPI1_Init();



  uart_driver_init(&uart_driver, &huart1);  //INICIA EL UART_DRIVER
  uint8_t startup_message[]= "+++TEMPERATURE CONTROL+++ \r\n";
  uart_driver_send(&uart_driver, startup_message, sizeof(startup_message)-1);
  //__HAL_TIM_SetCompare(&htim1,TIM_CHANNEL_1, 200);
  //HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);

  //uint32_t sensor_period_ms = 1000;
  //uint32_t led_tick=0;
  //float pwmstate=0;
  //uint8_t toggle_led_enabled = 0;

  //float Temperature_Degrees;
  ///////////**********SPI-TS*********////////////////
  uint8_t spi_tx_data_register=0;
  uint8_t spi_config_tx_data=0;
  uint8_t spi_config_rx_data=0;
  //uint8_t flagID=0;
  //float temperature_in_degrees=0;
  ////////////////////////////////////////////////////

  //////////********CONFIGURE POWER MODE SENSOR***********///////////////

  spi_tx_data_register = BMP280_REG_ADDR_CTRL_MEAS - 0x80; //Write register
  HAL_GPIO_WritePin(bmp280_cs_GPIO_Port, bmp280_cs_Pin, GPIO_PIN_RESET);
  if (HAL_SPI_Transmit(&hspi1, &spi_tx_data_register, 1, 100)==HAL_OK)
  {
	  spi_config_tx_data= 0b00100011; ///001 not temp oversampling ; 000  skip preassure; 11 normal power mode.
	  if(HAL_SPI_Transmit(&hspi1, &spi_config_tx_data, 1, 100)==HAL_OK)
	  {}
	  spi_tx_data_register = BMP280_REG_ADDR_CTRL_MEAS; //Read register
	  if (HAL_SPI_Transmit(&hspi1, &spi_tx_data_register, 1, 100) == HAL_OK)
	  {
		  if (HAL_SPI_Receive(&hspi1, &spi_config_rx_data, 1, 100) == HAL_OK)
		  {
		  }
	  }
  }
  HAL_GPIO_WritePin(bmp280_cs_GPIO_Port, bmp280_cs_Pin, GPIO_PIN_SET);
  ////////////////////////////////////////////////////////////////////




  ////////*************DMA_EXAMPLE_CLASS*************//////////
/*
#define BUFF_SIZE 1024
  uint16_t pwm_dma_buff[BUFF_SIZE]={0};
  for (uint16_t index=0; index<BUFF_SIZE; index++)
  {
	  if (index<300)
	  {
		  pwm_dma_buff[index]=10;
	  }
	  else if (index<600)
	  {
		  pwm_dma_buff[index]=200;
	  }
	  else if (index<800)
	  {
		  pwm_dma_buff[index]=500;
	  }
	  else
	  {
		  pwm_dma_buff[index]=999;
	  }

  }


HAL_TIMEx_PWMN_Start_DMA(&htim1, TIM_CHANNEL_1, (uint32_t *)pwm_dma_buff, BUFF_SIZE);*/
/////////////////////////////////////////////////////////////////////

 void door_state(void)	// detection of the User button to active or disactive the door
 {
 	uint16_t adc_val =0;
 	int cont =0;
 	if (HAL_ADC_PollForConversion(&hadc1, 10) == HAL_OK)
 	{
		adc_val = HAL_ADC_GetValue(&hadc1);

		if(adc_val > 2448)
		{
			HAL_Delay(500);
			if (cont==0)
			{

				printf("Door is open ");
				HAL_GPIO_WritePin(door_GPIO_Port, door_Pin, GPIO_PIN_SET);
				cont =1;
			}
			if (cont==1)
			{
				printf("Door is close ");
				HAL_GPIO_WritePin(door_GPIO_Port, door_Pin, GPIO_PIN_RESET);
				cont=0;
			}
		}
 	}
 }

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  uart_driver_run(&uart_driver);
	  /*
	  heartbeat();
	  door_state();
	  if ((HAL_GetTick()-led_tick)>sensor_period_ms)
	  {
		  led_tick=HAL_GetTick();
		  Temperature_Degrees = BMP280_read_temperature(); //llama función de leyendo temperatura y la almacena en Temperature_Degrees*/
		////////////**********LEYENDO  TEMP*****************/////////////
		 /* if (Temperature_Degrees!= 46)
		  {

			  if(Temperature_Degrees>12 && Temperature_Degrees<18)
			  {
				  printf ("T:  %f    FAN:OFF       HEATER:OFF   ALARM:OFF   MESSAGE: COLD \r\n",Temperature_Degrees);
				  HAL_GPIO_WritePin(cooler_GPIO_Port, cooler_Pin, GPIO_PIN_RESET);
				  HAL_GPIO_WritePin(heater_GPIO_Port, heater_Pin, GPIO_PIN_RESET);
				  HAL_GPIO_WritePin(alarm_led_GPIO_Port, alarm_led_Pin, GPIO_PIN_RESET);
				  pwmstate=0;

			  }
			  else if(Temperature_Degrees>0 && Temperature_Degrees<10)
			  {
				  printf ("T:  %f    FAN:OFF       HEATER:ON    ALARM:OFF   MESSAGE: VERY COLD \r\n",Temperature_Degrees);
				  HAL_GPIO_WritePin(cooler_GPIO_Port, cooler_Pin, GPIO_PIN_RESET);
				  HAL_GPIO_WritePin(heater_GPIO_Port, heater_Pin, GPIO_PIN_SET);
				  HAL_GPIO_WritePin(alarm_led_GPIO_Port, alarm_led_Pin, GPIO_PIN_RESET);
				  pwmstate=0;
			  }
			  else if(Temperature_Degrees>45 || Temperature_Degrees<0)
			  {
				  printf ("T:  %f    FAN:OFF       HEATER:OFF   ALARM:ON   MESSAGE: ALERT!! TEMPERATURE OUT OF CONTROL \r\n",Temperature_Degrees);
				  HAL_GPIO_WritePin(cooler_GPIO_Port, cooler_Pin, GPIO_PIN_RESET);
				  HAL_GPIO_WritePin(heater_GPIO_Port, heater_Pin, GPIO_PIN_RESET);
				  HAL_GPIO_WritePin(alarm_led_GPIO_Port, alarm_led_Pin, GPIO_PIN_SET);
				  pwmstate=0;
			  }
			  else if(Temperature_Degrees>40 && Temperature_Degrees<45)
			  {
				  printf ("T:  %f    FAN:ON 100%%   HEATER:OFF   ALARM:OFF   MESSAGE: VERY HOT \r\n",Temperature_Degrees);
				  //__HAL_TIM_SetCompare(&htim1,TIM_CHANNEL_1, 1000);
				  pwm_dma(pwmstate, 100);
				  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
				  HAL_GPIO_WritePin(heater_GPIO_Port, heater_Pin, GPIO_PIN_RESET);
				  HAL_GPIO_WritePin(alarm_led_GPIO_Port, alarm_led_Pin, GPIO_PIN_RESET);
				  pwmstate=100;
			  }
			  else if(Temperature_Degrees>30 && Temperature_Degrees<38)
			  {
				  printf ("T:  %f    FAN:ON 60%%    HEATER:OFF   ALARM:OFF   MESSAGE: HOT \r\n",Temperature_Degrees);
				  //__HAL_TIM_SetCompare(&htim1,TIM_CHANNEL_1, 600);
				  pwm_dma(pwmstate, 60);
				  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
				  HAL_GPIO_WritePin(heater_GPIO_Port, heater_Pin, GPIO_PIN_RESET);
				  HAL_GPIO_WritePin(alarm_led_GPIO_Port, alarm_led_Pin, GPIO_PIN_RESET);
				  pwmstate=60;

			  }
			  else if (Temperature_Degrees>20 && Temperature_Degrees<28)
			  {
				  printf ("T:  %f    FAN:ON 20%%    HEATER:OFF   ALARM:OFF   MESSAGE: MILD \r\n",Temperature_Degrees);
				  //__HAL_TIM_SetCompare(&htim1,TIM_CHANNEL_1, 200);
				  pwmstate=0;
				  pwm_dma(pwmstate, 20);
				  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
				  HAL_GPIO_WritePin(heater_GPIO_Port, heater_Pin, GPIO_PIN_RESET);
				  HAL_GPIO_WritePin(alarm_led_GPIO_Port, alarm_led_Pin, GPIO_PIN_RESET);
				  pwmstate=20;
			  }
		  }
	  }*/



	  /*			Example of class
	  if (HAL_ADC_PollForConversion(&hadc1, 10) == HAL_OK)
	  {
		  uint16_t adc_val = HAL_ADC_GetValue(&hadc1);
		  float voltage = 3.3 * (adc_val/4096.0);
		  printf ("ADC:  %d		Volt: %f \r\n",adc_val,voltage);
	  }
	  */
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }

}
  /* USER CODE END 3 */


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 50;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 90;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 1000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(bmp280_cs_GPIO_Port, bmp280_cs_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOF, door_Pin|heater_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, Heartbeat_led_Pin|alarm_led_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : bmp280_cs_Pin */
  GPIO_InitStruct.Pin = bmp280_cs_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(bmp280_cs_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : door_Pin heater_Pin */
  GPIO_InitStruct.Pin = door_Pin|heater_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : Heartbeat_led_Pin alarm_led_Pin */
  GPIO_InitStruct.Pin = Heartbeat_led_Pin|alarm_led_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
