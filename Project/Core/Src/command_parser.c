#include "command_parser.h"
#include "uart_driver.h"
#include "main.h"
#include <string.h>


/////////////COMMANDS///////////////////
extern uart_driver_t uart_driver;
const uint8_t read_temperature_cmd[] = "READ_TEMPERATURE";
const uint8_t read_fan_speed_cmd[] = "READ_FAN_SPEED";
const uint8_t set_fan_speed_cmd[] = "SET_FAN_SPEED";
const uint8_t close_door_cmd[] = "CLOSE_DOOR";
const uint8_t open_door_cmd[] = "OPEN_DOOR";
const uint8_t get_door_position_cmd[] = "GET_DOOR_POSITION";
const uint8_t get_fw_version_cmd[] = "GET_FW_VERSION";
const uint8_t get_unit_tick_cmd[] = "GET_UNIT_TICK";
const uint8_t get_heater_state_cmd[] = "GET_HEATER_STATE";




////////////////MESSAGES/////////////
const uint8_t ack_message[] = "*ACK#";
const uint8_t ack_message2[] = "*ACK_Sfs#";

const uint8_t state_door_open[] = "*D0#";
const uint8_t state_door_close[] = "*D1#";
const uint8_t ack_message6[] = "*V1.0.20220605#";

const uint8_t state_fan_0[] = "*F000#";
const uint8_t state_fan_25[] = "*F025#";
const uint8_t state_fan_50[] = "*F050#";
const uint8_t state_fan_75[] = "*F075#";
const uint8_t state_fan_100[] = "*F100#";

const uint8_t state_heater_on[] = "*H1#";
const uint8_t state_heater_off[] = "*H0#";
const uint8_t nack_message[] = "NACK";
const uint8_t preamble[] = "*";
const uint8_t posamble[] = "#";

uint8_t cont_fan=0;
uint8_t pwmstate=0;
float temperature;

void parse_command(uint8_t *rx_packet )
{
	if(memcmp(rx_packet, read_temperature_cmd, sizeof (read_temperature_cmd)-1)==0){
	/// LO QUE ME DEBE HACER
		uart_driver_send(&uart_driver,(uint8_t *)preamble, sizeof(preamble)-1);

		float temperature = BMP280_read_temperature();
		uint8_t temperature_sensor[6];
		sprintf(temperature_sensor, "%f", temperature);
		uart_driver_send(&uart_driver,(uint8_t *)temperature_sensor, sizeof(temperature_sensor)-1);
		//uart_driver_send(&uart_driver,(uint8_t *)posamble, sizeof(posamble));

	}else if (memcmp(rx_packet, read_fan_speed_cmd, sizeof (read_fan_speed_cmd)-1)==0){

		switch (pwmstate){
		case 0:
			uart_driver_send(&uart_driver,(uint8_t *)state_fan_0, sizeof(state_fan_0)-1);
			break;
		case 25:
			uart_driver_send(&uart_driver,(uint8_t *)state_fan_25, sizeof(state_fan_25)-1);
			break;
		case 50:
			uart_driver_send(&uart_driver,(uint8_t *)state_fan_50, sizeof(state_fan_50)-1);
			break;
		case 75:
			uart_driver_send(&uart_driver,(uint8_t *)state_fan_75, sizeof(state_fan_75)-1);
			break;
		case 100:
			uart_driver_send(&uart_driver,(uint8_t *)state_fan_100, sizeof(state_fan_100)-1);
			break;
		}

	}else if (memcmp(rx_packet, set_fan_speed_cmd, sizeof (set_fan_speed_cmd)-1)==0){

		cont_fan++;
		switch (cont_fan){
		case 0:
			pwm_dma(pwmstate, 0);
			pwmstate=0;
			break;
		case 1:
			pwm_dma(pwmstate, 25);
			pwmstate=25;
			break;
		case 2:
			pwm_dma(pwmstate, 50);
			pwmstate=50;
			break;
		case 3:
			pwm_dma(pwmstate, 75);
			pwmstate=75;
			break;
		case 4:
			pwm_dma(pwmstate, 100);
			pwmstate=100;

			break;
		case 5:
			pwmstate=0;
			cont_fan=0;
			break;

		}
		uart_driver_send(&uart_driver,(uint8_t *)ack_message, sizeof(ack_message)-1);

	}else if (memcmp(rx_packet, close_door_cmd, sizeof (close_door_cmd)-1)==0){
		HAL_GPIO_WritePin(door_GPIO_Port, door_Pin, GPIO_PIN_RESET);
		uart_driver_send(&uart_driver,(uint8_t *)ack_message, sizeof(ack_message)-1);

	}else if (memcmp(rx_packet, open_door_cmd, sizeof (open_door_cmd)-1)==0){
		HAL_GPIO_WritePin(door_GPIO_Port, door_Pin, GPIO_PIN_SET);
		uart_driver_send(&uart_driver,(uint8_t *)ack_message, sizeof(ack_message)-1);

	}else if (memcmp(rx_packet, get_door_position_cmd, sizeof (get_door_position_cmd)-1)==0){
		if(HAL_GPIO_ReadPin(door_GPIO_Port, door_Pin)==1){
			uart_driver_send(&uart_driver,(uint8_t *)state_door_open, sizeof(state_door_open)-1);
		}
		if(HAL_GPIO_ReadPin(door_GPIO_Port, door_Pin)==0)
		{
			uart_driver_send(&uart_driver,(uint8_t *)state_door_close, sizeof(state_door_close)-1);
		}

	}else if (memcmp(rx_packet, get_fw_version_cmd, sizeof (get_fw_version_cmd)-1)==0){
		uart_driver_send(&uart_driver,(uint8_t *)ack_message6, sizeof(ack_message6)-1);

	}else if (memcmp(rx_packet, get_unit_tick_cmd, sizeof (get_unit_tick_cmd)-1)==0){



		uint32_t TICKS=0;
		TICKS=HAL_GetTick();
		float dd = TICKS/8640000;
		uint8_t Cdd[40];
		sprintf(Cdd, "%f",dd);

		float hh = TICKS/360000;
		uint8_t Chh[40];
		sprintf(Chh, "%f",hh);

		int mm = TICKS/6000;
		uint8_t Cmm[40];
		sprintf(Cmm, "%f",mm);

		int ss = TICKS/100;
		uint8_t Css[40];
		sprintf(Css, "%f",ss);

		uint8_t datetime[40];
		strcat(Cmm,Css);
		strcat(Chh,Cmm);
		sprintf(datetime, "%f",Chh);
		//problems With this;
		uart_driver_send(&uart_driver,(uint8_t *)datetime, sizeof(datetime)-1);

	}else if (memcmp(rx_packet, get_heater_state_cmd, sizeof (get_heater_state_cmd)-1)==0){
		if(HAL_GPIO_ReadPin(heater_GPIO_Port, heater_Pin)==1){
			uart_driver_send(&uart_driver,(uint8_t *)state_heater_on, sizeof(state_heater_on)-1);
		}
		if(HAL_GPIO_ReadPin(door_GPIO_Port, door_Pin)==0)
		{
			uart_driver_send(&uart_driver,(uint8_t *)state_heater_off, sizeof(state_heater_off)-1);
		}

	}else {
		uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message)-1);
	}
}
