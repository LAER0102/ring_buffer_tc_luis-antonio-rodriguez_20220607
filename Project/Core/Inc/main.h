/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define User_button_Pin GPIO_PIN_0
#define User_button_GPIO_Port GPIOA
#define bmp280_sck_Pin GPIO_PIN_5
#define bmp280_sck_GPIO_Port GPIOA
#define bmp280_miso_Pin GPIO_PIN_6
#define bmp280_miso_GPIO_Port GPIOA
#define bmp280_mosi_Pin GPIO_PIN_7
#define bmp280_mosi_GPIO_Port GPIOA
#define bmp280_cs_Pin GPIO_PIN_4
#define bmp280_cs_GPIO_Port GPIOC
#define door_Pin GPIO_PIN_11
#define door_GPIO_Port GPIOF
#define heater_Pin GPIO_PIN_13
#define heater_GPIO_Port GPIOF
#define cooler_Pin GPIO_PIN_9
#define cooler_GPIO_Port GPIOE
#define Heartbeat_led_Pin GPIO_PIN_13
#define Heartbeat_led_GPIO_Port GPIOG
#define alarm_led_Pin GPIO_PIN_14
#define alarm_led_GPIO_Port GPIOG
/* USER CODE BEGIN Private defines */
void pwm_dma(float pwm_init, float pwm_fin);
float BMP280_read_temperature(void);
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
